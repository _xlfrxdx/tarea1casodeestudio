﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tarea1EstudioDeCaso.Common
{
    public class TiposObjetos
    {
        public class FechaCaducidad
        {
            public int Dia { get; set; }
            public int Mes { get; set; }
            public ushort Año { get; set; }
        }
        public class ComposicionAire
        {
            public float Porc_Nitrogeno { get; set; }
            public float Porc_Oxigeno { get; set; }
            public float Porc_VaporAgua { get; set; }
            public float Porc_Diox_Carbono { get; set; }
        }

    }
}
