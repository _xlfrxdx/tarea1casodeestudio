﻿using System;
using System.Collections.Generic;
using System.Text;
using static Tarea1EstudioDeCaso.Common.TiposObjetos;

namespace Tarea1EstudioDeCaso.Clases
{
    public class Productos
    {
        public FechaCaducidad Fecha_Ca { get; set; }
        public string Numero_Lot { get; set; }
        public string Pais_O { get; set; }
        public DateTime Fecha_Enva { get; set; }
    }
}
