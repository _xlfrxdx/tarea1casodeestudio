﻿using System;
using System.Collections.Generic;
using System.Text;
using static Tarea1EstudioDeCaso.Common.TiposObjetos;

namespace Tarea1EstudioDeCaso.Clases.TiposProductosCongelados
{
    public class ProductoCongeladoAire : Productos_Congelados
    {

        public ComposicionAire composicionAire { get; set; }
    }
}
